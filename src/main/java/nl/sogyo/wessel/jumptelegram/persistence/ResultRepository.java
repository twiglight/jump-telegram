package nl.sogyo.wessel.jumptelegram.persistence;

import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Repository for the results resulting of the different commands, mostly used for debug etc.
 *
 * Implementation gets injected by Spring
 */
public interface ResultRepository extends MongoRepository<Result, Long> {
    /**
     * Find a Result based on its updateId
     *
     * @param updateId the updateId to find
     * @return the corresponding Result object, if any
     */
    Result findByUpdateId(long updateId);

    /**
     * Gets the Result object with the highest updateId, used to get the last handled command to use for the getUpdates
     *
     * @return the corresponding Result object, if any
     */
    Result findFirstByOrderByUpdateIdDesc();

    /**
     * Finds the first 25 results in order based on updateId (descending)
     *
     * @return at most 25 results
     */
    List<Result> findFirst25ByOrderByUpdateIdDesc();

    /**
     * Finds the first 25 results based on the given command type
     *
     * @param content command type which to find
     * @return at most 25 results of the given command type
     */
    @Query("{ 'content.command': ?0 }")
    List<Result> findFirst25ByContent(String content);
}
