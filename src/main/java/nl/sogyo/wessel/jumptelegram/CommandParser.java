package nl.sogyo.wessel.jumptelegram;

import nl.sogyo.wessel.jumptelegram.commands.AbstractCommand;
import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Update;
import nl.sogyo.wessel.jumptelegram.persistence.ResultRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Parser for the commands
 */
@Component
public class CommandParser {

    /**
     * Map of commands and corresponding keys.
     *
     * Due to these being autowired the keys are based on the classname
     */
    @Autowired
    private Map<String, AbstractCommand> availableCommands;

    /**
     * The connection to the database.
     */
    @Autowired
    private ResultRepository repository;

    /**
     * Tries to parse each message with each command in the availableCommands map
     *
     * @param updates List of updates
     * @return results of the command in the messages
     */
    public List<Result> parse(List<Update> updates) {
        List<Result> results = updates.stream()
                .flatMap(update -> {
                    String text = update.getMessage().getText();

                    // for all the existing commands check if the text starts with that key
                    return availableCommands.keySet().stream()
                            .map(key -> {

                                // convert the autowired key ("xxxCommand") to a more user friendly text
                                String commandLiteral = '/' + key.substring(0, key.indexOf("Command"));
                                Result result = null;

                                // if the command is present execute it with the text after the command in the message
                                if (text.startsWith(commandLiteral)) {
                                    result = availableCommands
                                            .get(key)
                                            .execute(
                                                    text.substring(commandLiteral.length()+1),
                                                    update.getMessage()
                                            );

                                    result.setUpdateId(update.getUpdateId());

                                    //TODO: is this the right place to save the result?
                                    repository.save(result);
                                }

                                return result;
                            });
                })
                .filter(result -> result != null) // remove the messages with no valid command out of the list
                .collect(Collectors.toList());

        return results;
    }

    /**
     * Sets the available commands. Is unused at the moment, but is added
     * if I want to add the option for runtime commands
     *
     * @param availableCommands new availableCommands map
     */
    public void setAvailableCommands(Map<String, AbstractCommand> availableCommands) {
        this.availableCommands = availableCommands;
    }
}
