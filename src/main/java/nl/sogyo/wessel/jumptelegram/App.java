package nl.sogyo.wessel.jumptelegram;

import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.Response;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Update;
import nl.sogyo.wessel.jumptelegram.persistence.ResultRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main Spring application entrypoint
 */
@SpringBootApplication
@EnableScheduling
public class App {

    /**
     *  Parser which translates the given message to a command and executes said command
     */
    @Autowired
    private CommandParser parser;

    /**
     * RestTemplate extension which handles all http calls to the telegram API
     * (and other api's as it is a resttemplate)
     */
    @Autowired
    private TelegramHTTPCaller telegramAPI;

    /**
     * The connection to the database.
     */
    @Autowired
    private ResultRepository repository;

    /**
     * Goes through all messages and filters these on only the message types the bot can handle
     *
     * @param resp: List of updates (POJOs based on the getUpdates api endpoint)
     * @return results of the command in the messages
     * @throws Exception if the api returns a not okay
     */
    private List<Result> handleUpdates(Response<Update[]> resp) throws Exception {
        if (!resp.isOk()) {
            throw new Exception("Response is not okay....");
        }

        List<Update> messages = Arrays.stream(resp.getResult())
                .filter(update -> update.getMessage() != null) //TODO: support multiple type of messages (now only text)
                .collect(Collectors.toList());

        return parser.parse(messages);
    }

    /**
     * Gets the next batch of messages in the api queue.
     *
     * This is scheduled with the Spring Scheduler option to be executed ever 500 miliseconds
     */
    @Scheduled(fixedDelay = 500)
    public void update() {
        try {
            Result lastHandledCommand = repository.findFirstByOrderByUpdateIdDesc();

            Response<Update[]> updates;

            if (lastHandledCommand != null)
                updates = telegramAPI.getUpdates(
                        lastHandledCommand.getUpdateId() + 1
                );
            else
                updates = telegramAPI.getUpdates();

            handleUpdates(updates);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Entrypoint, which starts the Spring application and sets the scheduler, DI, DB connection, etc.
     *
     * @param args commandline parameters
     */
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
