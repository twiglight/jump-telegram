package nl.sogyo.wessel.jumptelegram.commands;

import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Message;

public interface Command {
    /**
     * Main method which gets called by the commandparser and contains the business logic per command
     *
     * @param params the message of the Message modified to nog contain the command identificator
     * @param msg the message which called the command
     * @return the result of the command
     */
    Result execute(String params, Message msg);
}
