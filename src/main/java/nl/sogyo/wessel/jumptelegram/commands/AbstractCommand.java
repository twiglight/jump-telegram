package nl.sogyo.wessel.jumptelegram.commands;

import nl.sogyo.wessel.jumptelegram.TelegramHTTPCaller;
import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.Content;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.FailureContent;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstraction of a AbstractCommand. Contains things like the telegramAPI object etc
 */
public abstract class AbstractCommand implements Command {

    /**
     * Used to send responses based on the result of the command. Also gets used to make http calls to other apis
     */
    @Autowired
    protected TelegramHTTPCaller telegramAPI;

    /**
     * Creates the result based on the response of the api, if this result contains a not okay it gets changed
     *
     * @param params the message of the Message modified to nog contain the command identificator
     * @param content the result of the executed command
     * @return the result of the command
     *
     */
    protected Result generateResult(String params, Content content) {
        Result result = new Result(true, content);

        try {
            telegramAPI.sendMessage(result.getContent());
        } catch (Exception e) {
            e.printStackTrace();

            result = new Result(false,
                    new FailureContent(
                            content.getChatId(),
                            e.getMessage(),
                            content.getCommand(),
                            params
                    )
            );
        }

        return result;
    }
}
