package nl.sogyo.wessel.jumptelegram.commands;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.MessageContent;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Message;
import org.springframework.stereotype.Component;

import java.net.URI;

/**
 * AbstractCommand which retrieves dad jokes
 */
@Component
public class JokeCommand extends AbstractCommand {

    private final URI DAD_JOKE_API_URL = URI.create("https://icanhazdadjoke.com/");

    /**
     * Gets a random joke from the api
     *
     * @param params the message of the Message modified to nog contain the command identificator
     * @param msg the message which called the command
     * @return random dad joke
     */
    @Override
    public Result execute(String params, Message msg) {
        ApiResponse response = telegramAPI.get(DAD_JOKE_API_URL, ApiResponse.class);

        MessageContent content = new MessageContent(
                response.getJoke(),
                "" + msg.getChat().getId(),
                "joke");

        return generateResult(params, content);
    }

    /**
     * Internal model of the (useful things in the) response of the Dad Joke api
     *
     * Has to be static so the JacksonMapper can instantiate it.
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    static class ApiResponse {
        private String id;
        private String joke;

        public ApiResponse() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getJoke() {
            return joke;
        }

        public void setJoke(String joke) {
            this.joke = joke;
        }
    }
}
