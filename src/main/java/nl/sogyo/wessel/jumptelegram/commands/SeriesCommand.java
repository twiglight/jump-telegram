package nl.sogyo.wessel.jumptelegram.commands;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.Content;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.FailureContent;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.MessageContent;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Message;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * AbstractCommand which returns data about series
 */
@Component
public class SeriesCommand extends AbstractCommand {

    private final URI TVMAZE_API_URL = URI.create("http://api.tvmaze.com/");

    private Map<String, Command> subCommands = new HashMap();

    {
        subCommands.put("show", (params, msg) -> {
            Show show = telegramAPI.get(TVMAZE_API_URL.resolve("singlesearch/shows?q=" + params), Show.class);

            Content content = new MessageContent(
                    show.getSummary().replaceAll("",""), // TODO: filter out the tags
                    msg.getChat().getId(),
                    "series: show"
//                    "HTML"
            );

            return generateResult(params, content);
        });

    }

    @Override
    public Result execute(String params, Message msg) {
        return subCommands.entrySet().stream().map(entry -> {
            if(params.startsWith(entry.getKey())) {
                return entry.getValue().execute(
                        params.substring(entry.getKey().length()+1),
                        msg
                );
            } else  return null;
        }).filter(r -> r != null).findFirst().orElse(new Result(false, new FailureContent()));
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class ApiResponse {
        private Show show;

        public ApiResponse() {
        }

        public Show getShow() {
            return show;
        }

        public void setShow(Show show) {
            this.show = show;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Show {
        private int id;
        private String name;
        private String[] genres;
        private Schedule schedule;
        private String summary;

        public Show() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String[] getGenres() {
            return genres;
        }

        public void setGenres(String[] genres) {
            this.genres = genres;
        }

        public Schedule getSchedule() {
            return schedule;
        }

        public void setSchedule(Schedule schedule) {
            this.schedule = schedule;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Schedule {
        private String time;
        private String[] days;

        public Schedule() {
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String[] getDays() {
            return days;
        }

        public void setDays(String[] days) {
            this.days = days;
        }
    }
}
