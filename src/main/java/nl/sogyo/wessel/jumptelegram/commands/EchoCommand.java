package nl.sogyo.wessel.jumptelegram.commands;

import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.MessageContent;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Message;

import org.springframework.stereotype.Component;

/**
 * AbstractCommand which echos the send message back to the chat (more or less for debug purposes).
 */
@Component
public class EchoCommand extends AbstractCommand {

    /**
     * Sends the senders message back
     *
     * @param params the message of the Message modified to nog contain the command identificator
     * @param msg the message which called the command
     * @return the senders content
     */
    @Override
    public Result execute(String params, Message msg) {
        MessageContent content = new MessageContent(
                params,
                "" + msg.getChat().getId(),
                "echo");

        return generateResult(params, content);
    }
}
