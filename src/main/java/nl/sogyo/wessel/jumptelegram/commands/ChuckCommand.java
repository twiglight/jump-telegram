package nl.sogyo.wessel.jumptelegram.commands;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.MessageContent;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Message;
import org.springframework.stereotype.Component;

import java.net.URI;

/**
 * AbstractCommand which retrieves Chuck Norris memes
 */
@Component
public class ChuckCommand extends AbstractCommand {

    private final URI CHUCK_NORRIS_API_URL = URI.create("https://api.chucknorris.io/jokes/random");

    /**
     * Get a random meme from the api
     *
     * @param params the message of the Message modified to nog contain the command identificator
     * @param msg the message which called the command
     * @return a random Chuck Norris meme
     */
    @Override
    public Result execute(String params, Message msg) {
        ApiResponse response = telegramAPI.get(CHUCK_NORRIS_API_URL, ApiResponse.class);

        MessageContent content = new MessageContent(
                response.getValue(),
                "" + msg.getChat().getId(),
                "chuck");

        return generateResult(params, content);
    }

    /**
     * Internal model of the (useful things in the) response of the Chuck Norris api
     *
     * Has to be static so the JacksonMapper can instantiate it.
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    static class ApiResponse {
        private String iconUrl;
        private String value;

        public ApiResponse() {
        }

        public String getIconUrl() {
            return iconUrl;
        }

        public void setIconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
