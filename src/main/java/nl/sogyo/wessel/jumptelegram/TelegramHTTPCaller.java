package nl.sogyo.wessel.jumptelegram;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sogyo.wessel.jumptelegram.model.telegram.Response;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.Content;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Message;
import nl.sogyo.wessel.jumptelegram.model.telegram.response.Update;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;


/**
 * Extension on the Spring RestTemplat, which contains some wrapper classes to make the http calls easier
 */
@Component
public class TelegramHTTPCaller extends RestTemplate{

    /**
     * The Telegram API url with bot key
     *
     * Get concatenated and injected by Spring from the application.properties
     */
    @Value("#{'${api.url}'.concat('${bot.key}')}")
    private String API_URL;

    /**
     * Headers which make it possible to make request to some https endpoints
     */
    private final HttpHeaders headers = new HttpHeaders();

    {
        headers.set("user-agent", "jump-telegram");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    }

    /**
     * Wrapper for the http calls to get updates
     *
     * @return all updates in the queue (max 100 items)
     */
    public Response<Update[]> getUpdates() {
        return getUpdates(Endpoints.UPDATE_ENDPOINT);
    }

    /**
     * Wrapper for the http calls to get updates with an offset
     *
     * @return all updates in the queue (max 100 items with a >= updateId)
     */
    public Response<Update[]> getUpdates(long offset) {
        return getUpdates(Endpoints.UPDATE_ENDPOINT, "?offset=" + offset, "&limit=" + 100);
    }

    /**
     * Wrapper for the actual post call to the telegram getUpdates API endpoint. Creates the
     * url and calls the correct http method
     *
     * @param path: the API endpoint location
     * @param params: the params given with the url
     * @return all updates in the queue based on the params
     */
    private Response<Update[]> getUpdates(String path, String...params) {
        //TODO: is this a post or a get as the params are in the url?
        URI url = URI.create(API_URL + path + Arrays.asList(params).stream().reduce((s, s2) -> s+=s2).get());
        return post(url, new ParameterizedTypeReference<Response<Update[]>>() {});
    }

    /**
     * Wrapper for the http call to send a message with the telegram API
     *
     * @param message the content which to send
     * @return the response (the message itself and status indication)
     * @throws Exception if the api response with a not okay
     */
    public Response<Message> sendMessage(Content message) throws Exception {
        Response<Message> response =
            this.exchange(URI.create(API_URL + Endpoints.MESSAGE_ENDPOINT),
                            HttpMethod.POST,
                            message,
                            new ParameterizedTypeReference<Response<Message>>() {});

        if (response.isOk())
            return response;
        else throw new Exception("Server responded with a not okay");
    }




    private class Endpoints {
        public static final String UPDATE_ENDPOINT = "/getUpdates";
        public static final String MESSAGE_ENDPOINT = "/sendMessage";
    }

    /*
     *
     * Some ease of use methods for http requests
     *
     */

    public <T> T post(URI url, Class<T> responseType) {
        return this.exchange(url, HttpMethod.POST, responseType);
    }

    public <T> T post(URI url, ParameterizedTypeReference<T> responseType) {
        return this.exchange(url, HttpMethod.POST, responseType);
    }

    public <T> T get(URI url, Class<T> responseType) {
        return this.exchange(url, HttpMethod.GET, responseType);
    }

    public <T> T get(URI url, ParameterizedTypeReference<T> responseType) {
        return this.exchange(url, HttpMethod.GET, responseType);
    }

    /*
    *
    *   Some wrapper functions to add the header to a request as it isn't possible
    *   to set a header in the RestTemplate for all the requests... right?
    *
    */

    public <T> T exchange(URI url, HttpMethod method, Class<T> responseType) throws RestClientException {
        HttpEntity<Object> headerEntity = new HttpEntity<>(null, headers);

        return super.exchange(url, method, headerEntity, responseType).getBody();
    }

    public <T> T exchange(URI url, HttpMethod method, Content body, Class<T> responseType) throws RestClientException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> params = mapper.convertValue(body, Map.class);

        HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);

        return super.exchange(url, method, entity, responseType).getBody();
    }

    public <T> T exchange(URI url, HttpMethod method, ParameterizedTypeReference<T> responseType) throws RestClientException {
        HttpEntity<Object> headerEntity = new HttpEntity<>(null, headers);

        return super.exchange(url, method, headerEntity, responseType).getBody();
    }

    public <T> T exchange(URI url, HttpMethod method, Content body, ParameterizedTypeReference<T> responseType) throws RestClientException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> params = mapper.convertValue(body, Map.class);

        HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);

        return super.exchange(url, method, entity, responseType).getBody();
    }
}
