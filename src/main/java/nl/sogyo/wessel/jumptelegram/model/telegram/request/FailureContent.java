package nl.sogyo.wessel.jumptelegram.model.telegram.request;

/**
 * Content that indicates that a command has failed at some point. Used more for debug options so it
 * contains reasons and the parameters which got handled
 */
public class FailureContent extends Content {
    private String reason;
    private String params;

    public FailureContent() {
        super();
    }

    public FailureContent(String chatId, String reason, String command, String params) {
        super(chatId, command);
        this.reason = reason;
        this.params = params;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "FailureContent{" +
                "reason='" + reason + '\'' +
                ", params='" + params + '\'' +
                '}';
    }
}
