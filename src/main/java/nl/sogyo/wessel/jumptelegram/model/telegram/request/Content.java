package nl.sogyo.wessel.jumptelegram.model.telegram.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * Abstraction of the content a response from this bot
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class Content {
    private String chatId;
    private String command;

    public Content() {
    }

    public Content(String chatId, String command) {
        this.chatId = chatId;
        this.command = command;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "Content{" +
                "command='" + command + '\'' +
                ", chatId='" + chatId + '\'' +
                '}';
    }
}
