package nl.sogyo.wessel.jumptelegram.model.telegram;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import nl.sogyo.wessel.jumptelegram.model.telegram.request.Content;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Object that contains the result of an executed command
 */
@Document(collection = "results")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Result {
    private boolean ok;
    private long updateId;
    private LocalDateTime dateTime;
    private Content content;

    public Result(boolean ok, Content content) {
        this.ok = ok;
        this.content = content;
        this.dateTime = LocalDateTime.now();
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(long updateId) {
        this.updateId = updateId;
    }

    public String getDateTime() {
        return dateTime.toString();
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Result{" +
                "ok=" + ok +
                ", updateId=" + updateId +
                ", content=" + content +
                '}';
    }
}
