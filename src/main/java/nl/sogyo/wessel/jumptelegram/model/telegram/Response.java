package nl.sogyo.wessel.jumptelegram.model.telegram;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * The response wrapper which the Telegram api sends each response in.
 * @param <T> Type of the content of the response (Update[], for /getUpdates or Message, for /sendMessage, for instance)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Response<T> {
    private boolean ok;
    private T result;

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T results) {
        this.result = results;
    }

    @Override
    public String toString() {
        return "Response{" +
                "ok=" + ok +
                ", results=" + result +
                '}';
    }
}
