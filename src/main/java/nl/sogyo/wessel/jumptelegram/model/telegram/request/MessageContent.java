package nl.sogyo.wessel.jumptelegram.model.telegram.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * Content which indicates a command was handled successfully, which contains a message
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MessageContent extends Content{
    private String text;
    private String parseMode;

    public MessageContent() {
        super();
    }

    public MessageContent(String text, String chatId, String command) {
        super(chatId, command);
        this.text = text;
    }

    public MessageContent(String text, long chatId, String command) {
        this(text, "" + chatId, command);
    }

    public MessageContent(String text, String chatId, String command, String parseMode) {
        this(text, chatId, command);
        this.parseMode = parseMode;
    }

    public MessageContent(String text, long chatId, String command, String parseMode) {
        this(text, "" + chatId, command, parseMode);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParseMode() {
        return parseMode;
    }

    public void setParseMode(String parseMode) {
        this.parseMode = parseMode;
    }

    @Override
    public String toString() {
        return "MessageContent{" +
                "text='" + text + '\'' +
                ", parseMode='" + parseMode + '\'' +
                '}';
    }
}
