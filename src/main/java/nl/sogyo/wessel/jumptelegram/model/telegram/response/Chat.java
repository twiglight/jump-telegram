package nl.sogyo.wessel.jumptelegram.model.telegram.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Representation of the chat a message of update was send in, could be a user in a one on one chat
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Chat {
    private long id;
    private String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
