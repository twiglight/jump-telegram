package nl.sogyo.wessel.jumptelegram.endpoints;

import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.persistence.ResultRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Api endpoints which returns the last 25 results of commands
 */
@RestController
public class HistoryController {

    /**
     * The connection to the database.
     */
    @Autowired
    private ResultRepository repository;

    /**
     * Exposes an api endpoint which return the last 25 handled command results
     * @return at most 25 results
     */
    @CrossOrigin
    @RequestMapping("/api/history")
    public List<Result> getHistory() {
        List<Result> results = repository.findFirst25ByOrderByUpdateIdDesc();
        return results;
    }

    /**
     * Exposes an api endpoint which return the last 25 handled command results with a common type
     * @return at most 25 results, all belonging to a certain type
     */
    @CrossOrigin
    @RequestMapping("/api/history/command")
    public List<Result> getHistoryByCommand(@RequestParam(name = "type") String command) {
        List<Result> results = repository.findFirst25ByContent(command);
        return results;
    }
}
