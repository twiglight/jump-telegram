package nl.sogyo.wessel.jumptelegram.endpoints;

import nl.sogyo.wessel.jumptelegram.model.telegram.Result;
import nl.sogyo.wessel.jumptelegram.persistence.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Api endpoints which expose certain statistics about the bot
 */
@RestController
public class StatisticController {

    /**
     * The connection to the database.
     */
    @Autowired
    private ResultRepository repository;

    /**
     * The list of statistic implementations which get used
     */
    private List<StatisticDescriptor> statisticDescriptors = new ArrayList<>();

    // Instead of defining seperate class which implement the StatisticDescriptor interface I can define lambda's
    // Due to this choice it is not possible (at least to my knowledge) to use Autowired ...
    {
        Collections.addAll(statisticDescriptors,
            r -> new Statistic("messages send", r.size()),
            r -> new Statistic("commands succeeded", r.stream().filter(o -> o.isOk()).count()),
            r -> new Statistic("commands failed", r.stream().filter(o -> !o.isOk()).count()),
            r -> new Statistic("latest command", r.stream().max(Comparator.comparingLong(Result::getUpdateId)).orElse(null)),
            r -> new Statistic("active chats", r.stream().map(result -> result.getContent().getChatId()).distinct().collect(Collectors.toList()))
        );
    }

    /**
     * Exposes an api endpoint which return the results of all statisticimplementations in the statisticDescriptors list
     * @return the statistics
     */
    @CrossOrigin
    @RequestMapping("/api/statistics")
    public List<Statistic> getStatistics() {
        List<Result> results = repository.findAll();

        return statisticDescriptors.stream()
                .map(descriptor -> descriptor.execute(results))
                .collect(Collectors.toList());
    }

    /**
     * Inner interface which gets implemented by the anonymous/lambda statisc implementations
     */
    private interface StatisticDescriptor {
        /**
         * Main method which gets called and contains the business logic to calculate the statistic
         * @param results all the results from the database
         * @return a statistic
         */
        Statistic execute(List<Result> results);
    }

    /**
     * Inner class which represents a statistic
     * @param <T> The type of the value of the statistic (like Message of a simple String for instance)
     */
    private class Statistic<T> {
        private String name;
        private T value;

        public Statistic(String name, T value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public T getValue() {
            return value;
        }
    }
}
