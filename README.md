# Jump Telegram
_A Java Spring implementation of a Telegram Bot_

## Dependencies

The Jump Telegram bot is dependent on a couple of things:
- Maven
- MongoDB
- Two valid keys in the application.properties in the resource folder:
    - bot.key
    - api.url